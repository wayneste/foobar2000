#ifndef _LIBSIDPLAYFP_CONFIG_H_
#define _LIBSIDPLAYFP_CONFIG_H_

#define HAVE_CXX11 1

#define HAVE_BOOL 1

#define HAVE_MMINTRIN_H 1

#define HAVE_EMMINTRIN_H 1

#define HAVE_STRICMP 1

#define HAVE_STRNICMP 1

#define stricmp _stricmp
#define strnicmp _strnicmp

#define PACKAGE_NAME "libsidplayfp"
#define PACKAGE_TARNAME "libsidplayfp"
#define PACKAGE_STRING "libsidplayfp 2.3.1"
#define PACKAGE_VERSION "2.3.1"
#define PACKAGE_URL "https://github.com/libsidplayfp/libsidplayfp/"

#ifndef VERSION
#define VERSION "2.3.1"
#endif

#endif
